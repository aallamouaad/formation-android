package excilys.formationchat.rest;

import excilys.formationchat.model.entities.Message;
import excilys.formationchat.model.entities.User;
import excilys.formationchat.rest.dtos.JsonResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by excilys on 02/05/16.
 */
public interface IChatApi {

    @GET("messages")
    Call<Message[]> getMessages(@Query("offset") int offset, @Query("limit") int limit);

    @GET("connect")
    Call<JsonResponse> connect();

    @GET("profile/{login}")
    Call<User> getProfile(@Path("login") String login);

    @POST("messages")
    Call<JsonResponse> sendMessage(@Body Message message);
}
