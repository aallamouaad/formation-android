package excilys.formationchat.rest.websocket;


import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Manager;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import excilys.formationchat.model.entities.Message;
import excilys.formationchat.model.entities.User;
import excilys.formationchat.rest.dtos.UserAuthDtoPush;

public class ChatWebSocketManager {
    public static final String API_WS_URL = "https://training.loicortola.com";
    public static final String SOCKET_IO_PATH = "/chat-rest/socket.io";
    public static final String SOCKET_IO_NSP = "/2.0/ws";
    public static final String OUTBOUND_MESSAGE = "outbound_msg";
    public static final String INBOUND_MESSAGE = "inbound_msg";
    public static final String POST_SUCCESS_MSG = "post_success_msg";
    public static final String AUTH_REQUIRED = "auth_required";
    public static final String AUTH_FAIL = "auth_failed";
    public static final String BAD_REQUEST_MSG = "bad_request_msg";
    public static final String AUTH_ATTEMPT = "auth_attempt";
    public static final String AUTH_SUCCESS = "auth_success";
    public static final String USER_TYPING_INBOUND_MSG = "user_typing_inbound_msg";
    public static final String USER_TYPING_OUTBOUND_MSG = "user_typing_outbound_msg";

    private static ChatWebSocketManager INSTANCE = null;

    private Gson mGson;
    private Socket mSocket;
    private Manager mManager;

    private OnChatInBoundMessageListener mOnChatInBoundMessage;
    private OnChatOutBoundMessageResponse mOnChatOutBoundMessageResponse;
    private OnChatWritingMessageListener mOnChatWritingMessageListener;

    private OnAuthentificationResponse mOnAuthentificationResponse;
    private String mToken;

    public static ChatWebSocketManager getInstance() {
        if (INSTANCE == null) INSTANCE = new ChatWebSocketManager();
        return INSTANCE;
    }

    private ChatWebSocketManager() {
        mGson = new GsonBuilder().serializeNulls().create();
        connect();
        listen();
    }

    public void connect() {
        try {
            Manager.Options options = new Manager.Options();
            options.path = SOCKET_IO_PATH;
            mManager = new Manager(new URI(API_WS_URL), options);
            mSocket = mManager.socket(SOCKET_IO_NSP);

        } catch (URISyntaxException e) {
            Log.e(ChatWebSocketManager.class.getSimpleName(), "connectInBoundMessage: " + e.toString());
        }
        mSocket.connect();
    }

    public void authenticate(String login, String password, OnAuthentificationResponse onAuthentificationResponse) {
        mOnAuthentificationResponse = onAuthentificationResponse;
        UserAuthDtoPush userAuthDto = new UserAuthDtoPush();
        userAuthDto.setLogin(login);
        userAuthDto.setPassword(password);
        String userAuthDtoJson = mGson.toJson(userAuthDto);
        Log.i(ChatWebSocketManager.class.getSimpleName(), "authenticate: " + userAuthDtoJson);

        mSocket.emit(AUTH_ATTEMPT, userAuthDtoJson);
    }

    public void listen() {
        /* AUTH REQUIRED */
        mSocket.on(AUTH_REQUIRED, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(ChatWebSocketManager.class.getSimpleName(), AUTH_REQUIRED);
            }
        });

        /* AUTH FAIL */
        mSocket.on(AUTH_FAIL, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(ChatWebSocketManager.class.getSimpleName(), AUTH_FAIL);
                if (mOnAuthentificationResponse != null) {
                    mOnAuthentificationResponse.onAuthentificationFailed();
                }
            }
        });

        /* AUTH SUCCESS */
        mSocket.on(AUTH_SUCCESS, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(ChatWebSocketManager.class.getSimpleName(), AUTH_SUCCESS);
                if (args.length > 0) {
                    JsonParser jsonParser = new JsonParser();
                    JsonObject jsonObject = jsonParser.parse(args[0].toString()).getAsJsonObject();
                    String token = jsonObject.get("token").getAsString();

                    if (mOnAuthentificationResponse != null) {
                        mOnAuthentificationResponse.onAuthentificationSuccess(token);
                    }
                }
            }
        });

        /* POST MESSAGE SUCCESS */
        mSocket.on(POST_SUCCESS_MSG, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(ChatWebSocketManager.class.getSimpleName(), POST_SUCCESS_MSG);
                if (mOnChatOutBoundMessageResponse != null) {
                    mOnChatOutBoundMessageResponse.onOutBoundMessageSuccess();
                }
            }
        });

        mSocket.on(BAD_REQUEST_MSG, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(ChatWebSocketManager.class.getSimpleName(), BAD_REQUEST_MSG);
            }
        });

        /* INBOUND MESSAGE */
        mSocket.on(INBOUND_MESSAGE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(ChatWebSocketManager.class.getSimpleName(), "call: " + INBOUND_MESSAGE + " args: " + Arrays.asList(args));
                if (args.length > 0) {
                    Message newMessage = mGson.fromJson(args[0].toString(), Message.class);
                    if (mOnChatInBoundMessage != null) {
                        mOnChatInBoundMessage.onInBoundMessage(newMessage);
                    }
                }
            }
        });

        /* USER TYPING INBOUND MESSAGE */
        mSocket.on(USER_TYPING_INBOUND_MSG, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(ChatWebSocketManager.class.getSimpleName(), "call: " + USER_TYPING_INBOUND_MSG + " args: " + Arrays.asList(args));
                if (args.length > 0) {
                    JsonParser jsonParser = new JsonParser();
                    JsonObject jsonObject = jsonParser.parse(args[0].toString()).getAsJsonObject();
                    String login = jsonObject.get("login").getAsString();
                    if (mOnChatWritingMessageListener != null) {
                        mOnChatWritingMessageListener.onWritingMessage(login);
                    }
                }
            }
        });
    }

    public void sendMessage(Message message, final OnChatOutBoundMessageResponse listener) {
        this.mOnChatOutBoundMessageResponse = listener;
        JsonObject jsonObject = new JsonParser().parse(mGson.toJson(message)).getAsJsonObject();
        jsonObject.addProperty("token", mToken);
        mSocket.emit(OUTBOUND_MESSAGE, jsonObject.toString());
    }

    public void sendTypingMessage(String login) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("login", login);
        jsonObject.addProperty("token", mToken);
        Log.i(ChatWebSocketManager.class.getSimpleName(), "sendTypingMessage: " + jsonObject);
        mSocket.emit(USER_TYPING_OUTBOUND_MSG, jsonObject.toString());
    }

    public void setOnChatInBoundMessageResponse(OnChatInBoundMessageListener mOnChatInBoundMessageListener) {
        this.mOnChatInBoundMessage = mOnChatInBoundMessageListener;
    }

    public void setOnChatWritingMessageListener(OnChatWritingMessageListener onChatWritingMessageListener) {
        this.mOnChatWritingMessageListener = onChatWritingMessageListener;
    }

    public void setToken(String token) {
        this.mToken = token;
    }

    public interface OnChatOutBoundMessageResponse {
        void onOutBoundMessageSuccess();
    }

    public interface OnAuthentificationResponse {
        void onAuthentificationSuccess(String token);
        void onAuthentificationFailed();
    }

    public interface OnChatInBoundMessageListener {
        void onInBoundMessage(Message inboundMessage);
    }

    public interface OnChatWritingMessageListener {
        void onWritingMessage(String login);
    }
}
