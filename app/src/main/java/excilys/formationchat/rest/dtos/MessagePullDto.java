package excilys.formationchat.rest.dtos;


import java.util.List;

import excilys.formationchat.model.entities.Attachment;

public class MessagePullDto {
    private String login;
    private String message;
    private String uuid;

    private List<Attachment> attachments;

    public MessagePullDto() {
    }
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    @Override
    public String toString() {
        return "Message{" +
                "login='" + login + '\'' +
                ", message='" + message + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
