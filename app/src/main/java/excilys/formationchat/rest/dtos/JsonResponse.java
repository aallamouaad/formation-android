package excilys.formationchat.rest.dtos;

public class JsonResponse {
    private int status;
    private String message;
    private String elements;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getElements() {
        return elements;
    }

    public void setElements(String elements) {
        this.elements = elements;
    }

    @Override
    public String toString() {
        return "JsonResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", elements='" + elements + '\'' +
                '}';
    }
}
