package excilys.formationchat.rest.dtos;

/**
 * Created by excilys on 03/05/16.
 */
public class UserAuthDtoPush {
    private String login;

    private String password;

    public UserAuthDtoPush() {

    }

    public UserAuthDtoPush(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
