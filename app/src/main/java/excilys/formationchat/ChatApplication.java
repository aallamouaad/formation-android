package excilys.formationchat;

import android.app.Application;

import excilys.formationchat.model.entities.User;
import excilys.formationchat.rest.IChatApi;
import excilys.formationchat.rest.ServiceGenerator;
import excilys.formationchat.rest.websocket.ChatWebSocketManager;

public class ChatApplication extends Application {
    public static final String AUTORIZATION = "Authorization";

    private ServiceGenerator mServiceGenerator;

    private ChatWebSocketManager mChatWebSocketManager;

    private IChatApi mChatService;

    private User mUser;

    @Override
    public void onCreate() {
        super.onCreate();
        mServiceGenerator = new ServiceGenerator();
        mChatWebSocketManager = ChatWebSocketManager.getInstance();
    }

    public ServiceGenerator getServiceGenerator() {
        return mServiceGenerator;
    }

    public IChatApi getChatService() {
        return mChatService;
    }

    public void setChatService(IChatApi mChatService) {
        this.mChatService = mChatService;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User mUser) {
        this.mUser = mUser;
    }

    public ChatWebSocketManager getChatWebSocketManager() {
        return mChatWebSocketManager;
    }
}
