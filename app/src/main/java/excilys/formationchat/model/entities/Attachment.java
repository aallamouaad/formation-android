package excilys.formationchat.model.entities;

/**
 * Created by tommy on 03/05/16.
 */
public class Attachment {
    private String mimeType;
    private String data;

    public Attachment() {

    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
