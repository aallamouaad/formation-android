package excilys.formationchat.model.entities;

/**
 * Created by buonomo on 01/05/16.
 */
public class User {
    private String login;
    private String email;

    private String password;

    private String picture;

    public String getLogin() {
        return login;
    }

    public void setLogin(String mLogin) {
        this.login = mLogin;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String mEmail) {
        this.email = mEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
