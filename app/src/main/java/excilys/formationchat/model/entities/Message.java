package excilys.formationchat.model.entities;

import java.util.List;

/**
 * Created by buonomo on 01/05/16.
 */
public class Message {
    private String login;
    private String message;
    private String uuid;
    private User user;

    private List<Attachment> attachments;

    public Message() {
    }
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Message{" +
                "login='" + login + '\'' +
                ", message='" + message + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
