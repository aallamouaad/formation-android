package excilys.formationchat.ui.fragments;

import android.animation.AnimatorSet;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import excilys.formationchat.ChatApplication;
import excilys.formationchat.R;
import excilys.formationchat.model.entities.Message;
import excilys.formationchat.rest.IChatApi;
import excilys.formationchat.rest.dtos.JsonResponse;
import excilys.formationchat.rest.websocket.ChatWebSocketManager;
import excilys.formationchat.ui.activities.MainActivity;
import excilys.formationchat.ui.adapters.ChatAdapter;
import excilys.formationchat.ui.factories.AnimatorFactory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatFragment extends Fragment implements ChatWebSocketManager.OnChatInBoundMessageListener, ChatWebSocketManager.OnChatWritingMessageListener {
    private MainActivity mMainActivity;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ChatAdapter mAdapter;
    private ChatApplication mChatApplication;
    private EditText mSendMessageEditText;
    private AnimatorSet mWritingAnimator;
    private LinearLayout mWritingLinearLayout;
    private Set<String> mWritingLogins;
    private TextView mWritingLoginsTextView;
    private Handler mHandler;

    private IChatApi mChatService;
    private ChatWebSocketManager mChatWebSocketManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chat, container, false);
        mMainActivity = (MainActivity) getActivity();
        mChatApplication = (ChatApplication) mMainActivity.getApplication();
        ChatApplication chatApplication = (ChatApplication) getActivity().getApplication();
        mChatService = chatApplication.getChatService();

        mChatWebSocketManager = chatApplication.getChatWebSocketManager();
        mChatWebSocketManager.setOnChatInBoundMessageResponse(this);
        mChatWebSocketManager.setOnChatWritingMessageListener(this);

        setUpToobar();
        setUpRecyclerView(rootView);
        setUpFragmentUI(rootView);
        getChatMessagesRequest();

        return rootView;
    }

    private void setUpToobar() {
        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mMainActivity.disableCollapsing(true);
        mMainActivity.getSupportActionBar().setTitle(getActivity().getResources().getString(R.string.fragment_chat_title));
    }

    private void getChatMessagesRequest() {
        Call<Message[]> call = mChatService.getMessages(0, 10);
        call.enqueue(new Callback<Message[]>() {
            @Override
            public void onResponse(Call<Message[]> call, Response<Message[]> response) {
                Log.i(ChatFragment.class.getSimpleName(), "onResponse: "+ Arrays.asList(response.body()));
                mAdapter.addMessages(Arrays.asList(response.body()));
            }

            @Override
            public void onFailure(Call<Message[]> call, Throwable t) {

            }
        });
    }

    private void sendMessageRequest(String messageString) {
        final Message message = new Message();
        message.setMessage(messageString);
        message.setLogin(mChatApplication.getUser().getLogin());
        message.setUuid(UUID.randomUUID().toString());

        //Using Web Socket API
//        mChatWebSocketManager.sendMessage(message, new ChatWebSocketManager.OnChatOutBoundMessageResponse() {
//            @Override
//            public void onOutBoundMessageSuccess() {
//                Log.i(ChatFragment.class.getSimpleName(), "onResponseSuccess: ");
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        mAdapter.addMessage(message);
//                        mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
//                    }
//                });
//            }
//        });

        Call<JsonResponse> call = mChatService.sendMessage(message);

        call.enqueue(new Callback<JsonResponse>() {
            @Override
            public void onResponse(Call<JsonResponse> call, Response<JsonResponse> response) {
                Log.i(ChatFragment.class.getSimpleName(), "onResponse: " + response.body());
                mAdapter.addMessage(message);
            }

            @Override
            public void onFailure(Call<JsonResponse> call, Throwable t) {
                Log.e(ChatFragment.class.getSimpleName(), "onFailure: " + t.toString());
            }
        });
    }

    private void setUpRecyclerView(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.chat_recycler_view);
        // use a Grid layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        /*To reverse order of elements*/
        mLayoutManager.setStackFromEnd(true);

        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ChatAdapter(new ArrayList<Message>(), (ChatApplication) mMainActivity.getApplication());
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setUpFragmentUI(View rootView) {
        mSendMessageEditText = (EditText) rootView.findViewById(R.id.chat_write_message_edit_text);

        ImageView sendMessageImageView = (ImageView) rootView.findViewById(R.id.chat_send_message_image_view);
        sendMessageImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSendMessageEditText.getText().toString().trim().isEmpty()) {
                    sendMessageRequest(mSendMessageEditText.getText().toString());
                    mSendMessageEditText.setText("");
                }
            }
        });

        mSendMessageEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    if (!mSendMessageEditText.getText().toString().trim().isEmpty()) {
                        sendMessageRequest(mSendMessageEditText.getText().toString());
                        mSendMessageEditText.setText("");
                    }
                    return true;
                }
                return false;
            }
        });
        
        mSendMessageEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    mChatWebSocketManager.sendTypingMessage(mChatApplication.getUser().getLogin());
                }
                return false;
            }
        });

        View point1 = rootView.findViewById(R.id.chat_writing_point1);
        View point2 = rootView.findViewById(R.id.chat_writing_point2);
        View point3 = rootView.findViewById(R.id.chat_writing_point3);

        mWritingLoginsTextView = (TextView) rootView.findViewById(R.id.chat_writing_text_view);
        mWritingLinearLayout = (LinearLayout) rootView.findViewById(R.id.chat_writing_layout);
        mWritingLinearLayout.setVisibility(View.GONE);
        mWritingAnimator = AnimatorFactory.createWritingPointAnimator(point1, point2, point3);
        mWritingLogins = new HashSet<>();
        mHandler = new Handler();
    }

    @Override
    public void onInBoundMessage(final Message inboundMessage) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!mChatApplication.getUser().getLogin().equals(inboundMessage.getLogin())){
                    mAdapter.addMessage(inboundMessage);
                    mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
                }
            }
        });
    }

    private void stopWritingAnimation() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mWritingAnimator.cancel();
                mWritingLinearLayout.setVisibility(View.GONE);
            }
        });
    }

    private void startWritingAnimation(String login) {
        mWritingLogins.add(login);
        mWritingLinearLayout.setVisibility(View.VISIBLE);
        if (!mWritingAnimator.isRunning()) {
            mWritingAnimator.start();
        }
        mWritingLoginsTextView.setText(login);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopWritingAnimation();
            }
        }, 3000);
    }

    @Override
    public void onWritingMessage(final String login) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                startWritingAnimation(login);
            }
        });
    }
}
