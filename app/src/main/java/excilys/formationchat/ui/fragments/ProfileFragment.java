package excilys.formationchat.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import excilys.formationchat.ChatApplication;
import excilys.formationchat.R;
import excilys.formationchat.model.entities.User;
import excilys.formationchat.ui.activities.MainActivity;

public class ProfileFragment extends Fragment {
    private MainActivity mMainActivity;
    private User mUser;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        mMainActivity = (MainActivity) getActivity();
        mUser = ((ChatApplication) mMainActivity.getApplication()).getUser();

        setupCollapsingToolbarLayout(rootView);

        return rootView;
    }

    private void setupCollapsingToolbarLayout(View rootView) {
        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.enableCollapsing(true);
        mMainActivity.getSupportActionBar().setTitle(mUser.getEmail());
    }
}
