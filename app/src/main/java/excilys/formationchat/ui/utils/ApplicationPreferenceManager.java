package excilys.formationchat.ui.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class ApplicationPreferenceManager {
    private static ApplicationPreferenceManager INSTANCE;
    private SharedPreferences mSharedPreferences;

    public final static String ACCESS_TOKEN_KEY = "access_token";
    public final static String USER_PARAM = "user";

    public static ApplicationPreferenceManager getInstance(Context context){
        if (INSTANCE == null) INSTANCE = new ApplicationPreferenceManager(context);
        return INSTANCE;
    }

    private ApplicationPreferenceManager (Context context){
        this.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public SharedPreferences getSharedPreferences (){
        return mSharedPreferences;
    }

    public void putIntPreference (String key, int value){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public void putStringPreference (String key, String value){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void clearCache(){
        mSharedPreferences.edit().clear().commit();
    }
}
