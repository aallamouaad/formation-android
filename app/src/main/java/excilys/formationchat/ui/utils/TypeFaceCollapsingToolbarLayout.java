package excilys.formationchat.ui.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.design.widget.CollapsingToolbarLayout;
import android.util.AttributeSet;
import android.util.Log;

import excilys.formationchat.R;

public class TypeFaceCollapsingToolbarLayout extends CollapsingToolbarLayout {
    public TypeFaceCollapsingToolbarLayout(Context context) {
        super(context);
    }

    public TypeFaceCollapsingToolbarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TypeFaceCollapsingToolbarLayout, 0, 0);
        setCustomFont(context,
                a.getString(R.styleable.TypeFaceCollapsingToolbarLayout_collapsed_typeface),
                a.getString(R.styleable.TypeFaceCollapsingToolbarLayout_expanded_typeface));
        a.recycle();
    }

    public TypeFaceCollapsingToolbarLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TypeFaceCollapsingToolbarLayout, 0, 0);
        setCustomFont(context,
                a.getString(R.styleable.TypeFaceCollapsingToolbarLayout_collapsed_typeface),
                a.getString(R.styleable.TypeFaceCollapsingToolbarLayout_expanded_typeface));
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String collapsedTypeface, String expandedTypeface) {
        Typeface tf1 = null;
        Typeface tf2 = null;
        try {
            tf1 = Typeface.createFromAsset(ctx.getAssets(), collapsedTypeface);
            tf2 = Typeface.createFromAsset(ctx.getAssets(), expandedTypeface);
        } catch (Exception e) {
            Log.e(TypeFaceCollapsingToolbarLayout.class.getSimpleName(), "Could not get typeface: " + e.getMessage());
            return false;
        }
        setCollapsedTitleTypeface(tf1);
        setExpandedTitleTypeface(tf2);
        return true;
    }

}
