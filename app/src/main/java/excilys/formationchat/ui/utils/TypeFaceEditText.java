package excilys.formationchat.ui.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

import excilys.formationchat.R;

public class TypeFaceEditText extends EditText {
    private static final String TAG = "TypeFaceEditText";

    public TypeFaceEditText(Context context) {
        super(context);
    }

    public TypeFaceEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TypeFaceTextView, 0, 0);
        setCustomFont(context, a.getString(R.styleable.TypeFaceTextView_typeface));
        a.recycle();
    }

    public TypeFaceEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TypeFaceTextView, 0, 0);
        setCustomFont(context, a.getString(R.styleable.TypeFaceTextView_typeface));
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String typeface) {
        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(ctx.getAssets(), typeface);
        } catch (Exception e) {
            Log.e(TAG, "Could not get typeface: " + e.getMessage());
            return false;
        }

        setTypeface(tf);
        return true;
    }
}