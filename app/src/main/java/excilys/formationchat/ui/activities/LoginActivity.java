package excilys.formationchat.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import excilys.formationchat.ChatApplication;
import excilys.formationchat.R;
import excilys.formationchat.model.entities.User;
import excilys.formationchat.rest.IChatApi;
import excilys.formationchat.rest.dtos.JsonResponse;
import excilys.formationchat.rest.ServiceGenerator;
import excilys.formationchat.rest.websocket.ChatWebSocketManager;
import excilys.formationchat.ui.utils.ApplicationPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */
public class LoginActivity extends AppCompatActivity {
    public static final String USER_NAME_PARAM = "DEBUG_ERROR";
    private EditText mUserNameEditText, mPasswordEditText;
    private FloatingActionButton mConnectButton;

    private ProgressDialog mProgressDialog;

    private ChatApplication mChatApplication;

    private Gson mGson;
    private IChatApi mChatService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //AndroidUtils.hideStatusBar(this);
        setContentView(R.layout.activity_login);
        mChatApplication = (ChatApplication) getApplication();
        mGson = new Gson();
        bindActivity();
    }

    private void bindActivity() {
        mUserNameEditText = (EditText) findViewById(R.id.username_edit_text);
        mPasswordEditText = (EditText) findViewById(R.id.password_edit_text);

        TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    if (!mUserNameEditText.getText().toString().trim().isEmpty() &&
                            !mPasswordEditText.getText().toString().trim().isEmpty()) {
                        connect(mUserNameEditText.getText().toString(), mPasswordEditText.getText().toString());
                    }
                    return true;
                }
                return false;
            }
        };

        mUserNameEditText.setOnEditorActionListener(editorActionListener);
        mPasswordEditText.setOnEditorActionListener(editorActionListener);

        mConnectButton = (FloatingActionButton) findViewById(R.id.send_button);
        mConnectButton.setActivated(false);

        mConnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mUserNameEditText.getText().toString().trim().isEmpty() &&
                        !mPasswordEditText.getText().toString().trim().isEmpty()) {
                    connect(mUserNameEditText.getText().toString(), mPasswordEditText.getText().toString());
                }
            }
        });

        String userJson = ApplicationPreferenceManager.getInstance(getApplicationContext())
                .getSharedPreferences()
                .getString(ApplicationPreferenceManager.USER_PARAM, null);
        if (userJson != null) {
            User user = mGson.fromJson(userJson, User.class);
            connect(user.getLogin(), user.getPassword());
        }
    }

    /**
     *
     */
    private void connect(final String username, final String password) {
        mConnectButton.setActivated(false);
        mProgressDialog = ProgressDialog.show(this, "Authentification", "Please wait...");

        mChatApplication.getChatWebSocketManager().authenticate(username, password, new ChatWebSocketManager.OnAuthentificationResponse() {
            @Override
            public void onAuthentificationSuccess(String token) {
                mChatApplication.getChatWebSocketManager().setToken(token);
            }

            @Override
            public void onAuthentificationFailed() {
            }
        });

        mChatService = ServiceGenerator.createService(IChatApi.class, username, password);
        Call<JsonResponse> call = mChatService.connect();
        call.enqueue(new Callback<JsonResponse>() {
            @Override
            public void onResponse(Call<JsonResponse> call, Response<JsonResponse> response) {
                if (response.code() == 200) {
                    Log.i(LoginActivity.class.getSimpleName(), "onResponse: OK");
                    mChatApplication.setChatService(mChatService);
                    saveUserPreference(username, password);
                    startMainActivity();
                }

                else {
                    Snackbar.make(mConnectButton, "Wrong login or password", Snackbar.LENGTH_LONG).show();
                }

                mConnectButton.setActivated(true);
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonResponse> call, Throwable t) {
                Log.e(LoginActivity.class.getSimpleName(), "onFailure: "+t.toString());
            }
        });
    }

    private void saveUserPreference(final String username, final String password) {
        final User user = new User();
        user.setLogin(username);
        user.setPassword(password);
        mChatApplication.setUser(user);
        ApplicationPreferenceManager.getInstance(getApplicationContext())
                .putStringPreference(ApplicationPreferenceManager.USER_PARAM, mGson.toJson(user));
    }


    /**
     * Check if username or password fields are empty, and active
     * connect button.
     */
    private void checkEmptyFields() {
        if (mUserNameEditText.getText().toString().trim().isEmpty() ||
                mPasswordEditText.getText().toString().trim().isEmpty() ||
                mConnectButton.getVisibility() == View.VISIBLE) {
            mConnectButton.setActivated(false);
        }

        else if (!mUserNameEditText.getText().toString().trim().isEmpty() &&
                !mPasswordEditText.getText().toString().trim().isEmpty() &&
                mConnectButton.getVisibility() == View.GONE) {
            mConnectButton.setActivated(true);
        }
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(LoginActivity.class.getSimpleName(), "onRestoreInstanceState: ");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(LoginActivity.class.getSimpleName(), "onSaveInstanceState: ");
    }
}
