package excilys.formationchat.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import excilys.formationchat.ChatApplication;
import excilys.formationchat.R;
import excilys.formationchat.model.entities.Message;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private List<Message> mDataset;

    private ChatApplication mChatApplication;

    private String mMainUserName;

    private Context mContext;
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView mUserNameTextView, mMessageTextView;
        CardView mMessageCardView;
        public ViewHolder(View rootView) {
            super(rootView);
            mUserNameTextView = (TextView) rootView.findViewById(R.id.item_message_username_text_view);
            mMessageTextView = (TextView) rootView.findViewById(R.id.item_message_text_view);
            mMessageCardView = (CardView) rootView.findViewById(R.id.item_message_card_view);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ChatAdapter(List<Message> myDataset, ChatApplication chatApplication) {
        this.mDataset = myDataset;
        this.mChatApplication = chatApplication;
        this.mMainUserName = chatApplication.getUser().getLogin();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View rootView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_message, parent, false);
        mContext = parent.getContext();
        // set the view's size, margins, paddings and layout parameters
        ViewHolder viewHolder = new ViewHolder(rootView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.i(ChatAdapter.class.getSimpleName(), "onBindViewHolder: "+position);
        String login = mDataset.get(position).getLogin();
        String message = mDataset.get(position).getMessage();

        holder.mMessageTextView.setText(message);
        setUpGrouping(holder, login, position);
        setUpMessagePosition(holder, login);
    }

    private void setUpGrouping(ViewHolder holder, String login, int position) {
        if (position == 0){
            if (!isCurrentUser(login)){
                holder.mUserNameTextView.setVisibility(View.VISIBLE);
                holder.mUserNameTextView.setText(login);
            }
            return;
        }

        Message previousMessage = mDataset.get(position-1);
        if (!isCurrentUser(login) && !previousMessage.getLogin().equals(login)) {
            holder.mUserNameTextView.setVisibility(View.VISIBLE);
            holder.mUserNameTextView.setText(login);
        }
        else {
            holder.mUserNameTextView.setVisibility(View.GONE);
        }
    }

    private void setUpMessagePosition(ViewHolder holder, String login) {
        LinearLayout.LayoutParams cardViewParams = new LinearLayout.LayoutParams(holder.mMessageTextView.getLayoutParams());
        int padding = (int) mChatApplication.getResources().getDimension(R.dimen.item_message_padding);

        if (isCurrentUser(login)) {
            cardViewParams.leftMargin = padding;
            cardViewParams.gravity = Gravity.END;
            holder.mMessageTextView.setBackgroundColor(ContextCompat.getColor(mChatApplication.getApplicationContext(), R.color.colorPrimary));
            holder.mMessageTextView.setTextColor(Color.WHITE);
        }

        else {
            cardViewParams.rightMargin = padding;
            cardViewParams.leftMargin = 0;
            cardViewParams.gravity = Gravity.START;
            holder.mMessageTextView.setBackgroundColor(Color.WHITE);
            holder.mMessageTextView.setTextColor(Color.GRAY);
        }
        holder.mMessageCardView.setLayoutParams(cardViewParams);
    }

    public void addMessages(List<Message> messages) {
        mDataset.addAll(messages);
        notifyDataSetChanged();
    }

    public void addMessage(Message message) {
        mDataset.add(message);
        notifyDataSetChanged();
    }

    private boolean isCurrentUser(String login) {
        return login.equals(mMainUserName);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
